import numpy as np
import matplotlib.pyplot as plt


def show_som(arr: np.array):
    fig = plt.figure()
    plt.clf()
    ax = fig.add_subplot(111)
    ax.set_aspect(1)
    res = ax.imshow(np.array(arr), cmap=plt.cm.jet,
                    interpolation='nearest')

    width, height = arr.shape

    for x in range(width):
        for y in range(height):
            ax.annotate(str(arr[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center')

    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    plt.xticks(range(width), alphabet[:width])
    plt.yticks(range(height), alphabet[:height])
    plt.show()
