from network import KohonenNN
import numpy as np
import dataset as ds
import map as m

size = 5
arr = np.zeros((size, size))

network = KohonenNN(7, size * size)

for i in range(1000):
    for p in ds.data:
        network.study(p)
    network.next_era()

clusters = {}
ind = 1

for p in ds.data:
    cluster = network.process(p)
    try:
        clusters[cluster] += p[ind]
    except KeyError:
        clusters[cluster] = p[ind]

print(clusters)
for key, val in clusters.items():
    row = key // size
    col = key - size * row
    arr[row][col] = val

m.show_som(arr)
