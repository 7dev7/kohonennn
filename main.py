from network import KohonenNN
import dataset as ds

network = KohonenNN(7, 3)

for i in range(1000):
    for p in ds.data:
        network.study(p)
    network.next_era()

for p in ds.data:
    cluster = network.process(p)
    print("Cluster: " + str(cluster) + " for " + str(p))
