import random
import math


def gaussian(x, sigma=0.5, mu=0):
    return 1 / (sigma * math.sqrt(2 * math.pi)) * math.exp(-(x - mu) * (x - mu) / (2 * sigma * sigma))


def hyperbolic(x, a=1, b=1):
    return a / (x + b)


def euclidean_distance(in_vector: list, out_vector: list) -> float:
    s = 0
    for i in range(len(in_vector)):
        x1 = in_vector[i]
        x2 = out_vector[i]
        s += ((x1 - x2) ** 2)
    return math.sqrt(s)


class Neuron:
    def __init__(self, index: int, input_vector_size: int):
        self.index = index
        self.potential = 1
        self.weights = []
        for i in range(input_vector_size):
            self.weights.append(random.random())


class KohonenNN:
    def __init__(self, input_vector_size: int, clusters: int):
        self.potential_minimum = 1
        self.input_vector_size = input_vector_size
        self.clusters = clusters
        self.distance_function = euclidean_distance
        self.neurons = []
        self.studying_era = 0
        for i in range(self.clusters):
            self.neurons.append(Neuron(i, self.input_vector_size))

    def __get_winner(self, vector: list, filter_potentials: bool):
        neuron_winner = self.neurons[0]
        minimum = self.distance_function(neuron_winner.weights, vector)
        for i in self.neurons:
            if not filter_potentials or i.potential - self.potential_minimum > -0.001:
                dist = self.distance_function(i.weights, vector)
                if dist < minimum:
                    neuron_winner = i
                    minimum = dist
        return neuron_winner

    def study(self, vector: list):
        winner = self.__get_winner(vector, True)

        for neuron in self.neurons:
            dist_to_win = self.distance_function(neuron.weights, winner.weights)
            power = gaussian(dist_to_win) * hyperbolic(self.studying_era)

            for i in range(len(neuron.weights)):
                delta = vector[i] - neuron.weights[i]
                neuron.weights[i] += power * delta

            neuron.potential += 1 / len(self.neurons)
            if winner.index == neuron.index:
                neuron.potential -= self.potential_minimum

    def next_era(self):
        self.studying_era += 1

    def process(self, vector: list):
        winner = self.__get_winner(vector, False)
        return winner.index
